
<?php 

   require_once __DIR__. "/autoload/autoload.php";
   $user = $db->fetchID("users",intval($_SESSION['name_id']));
   if($_SERVER["REQUEST_METHOD"]=="POST")
   {
   	  $data = 
   	  [
   	  	'amount'   => $_SESSION['total'],
   	  	'users_id' =>$_SESSION['name_id'],
   	  ];
   
   $idtransaction = $db-> insert("transaction",$data);
   if($idtransaction > 0)
   {
   	 foreach($_SESSION['cart'] as $key=> $value)
   	 {
   	 	$data2 = 
   	 	[
   	 		'transaction_id' => $idtransaction,
   	 		'product_id'     =>$key,
   	 		'qty'            =>$value['qty'],
   	 		'price'          =>$value['price'],
   	 	];
   	 	$id_insert = $db->insert("orders",$data2);
   	 }
   	  $_SESSION['success'] = "Lưu thành công";
   	  header("location: thongbao.php");
   }
}

 ?>
 <?php require_once __DIR__. "/layouts/header.php"; ?>

      <div class="col-md-9 bor">

                        <section class="box-main1">
                            <h3 class="title-main"><a href=""> Thông tin chi tiết</a> </h3>
                            <form action="" method="POST" class="form-horizontal formcustome" role= "form" style="margin-top: 20px">
                            	<div class="form-group">
                            		<label class="col-md-2 col-md-offset-1" > Tên Khách Hàng</label>
                                    <div class="col-md-8">
                                    	<input type="text" readonly="" name="name" placeholder="" class="form-control" value="<?php echo $user['name'] ?>">
                                    </div>
                            	</div>

                            	<div class="form-group">
                            		<label class="col-md-2 col-md-offset-1" > Email</label>
                                    <div class="col-md-8">
                                    	<input type="email" readonly="" name="email" placeholder="" class="form-control" value="<?php echo $user['email'] ?>">
                                    </div>
                            	</div>

                            	<div class="form-group">
                            		<label class="col-md-2 col-md-offset-1" > Số điện thoại</label>
                                    <div class="col-md-8">
                                    	<input type="number" readonly="" name="phone" placeholder="" class="form-control" value="<?php echo $user['phone'] ?>">
                                    </div>
                            	</div>

                            	<div class="form-group">
                            		<label class="col-md-2 col-md-offset-1" > Địa chỉ</label>
                                    <div class="col-md-8">
                                    	<input type="text" readonly="" name="address" placeholder="" class="form-control" value="<?php echo $user['address'] ?>">
                                    </div>
                            	</div>
                            	<div class="col-md-11" id = "thanhtoan">
                            	<ul class="list-group">
                                    
                                    <li class="list-group-item">
                                        <span class="badge"><?php echo formatPrice($_SESSION['tongtien']) ?></span>
                                        Số tiền
                                    </li>

                                    <li class="list-group-item">
                                        <span class="badge">10%</span>
                                        Thuế VAT
                                    </li>

                                    <li class="list-group-item">
                                        <span class="badge"><?php  $_SESSION['total'] = $_SESSION['tongtien']*110/100 ; echo formatPrice($_SESSION['total']) ?></span>
                                        Tổng tiền thanh toán
                                    </li> 
                                    </ul>
                                 </div>
                            	<button type="submit" class="btn btn-primary col-md-2 col-md-offset-9" style="margin-bottom: 20px;">Xác nhận</button>
                            </form>
                        </section>

          </div>
 <?php require_once __DIR__. "/layouts/footer.php"; ?>             

                