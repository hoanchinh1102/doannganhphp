
<?php 

   require_once __DIR__. "/autoload/autoload.php";

   if(isset($_SESSION['name_id']))
   {
    echo "<script>alert('Tài khoản đang duy trì bạn không thể vào đăng kí được');location.href='index.php'</script>";
   }

  $data = 
  [
    'name' => postInput("name"),
    'email' => postInput("email"),
    'password' => (postInput("password")),
    'phone' => postInput("phone"),
    'address'=> postInput("address")
  ];

   $name = $email = $password = $phone = $address = '';
   $error = [];
   if($_SERVER["REQUEST_METHOD"]=="POST")
   {
     
     if($data['name'] == '')
     {
      $error['name'] = "Mời bạn nhập tên !";
     }

    
     if($data['email'] == '')
     {
      $error['email'] = "Mời bạn nhập email !";
     }
     else
     {
      $is_check = $db->fetchOne("users","email = '".$data['email']."' "); 
      if($is_check !=NULL)
      {
        $error['email'] = "Email đã tồn tại mời bạn nhập email khác";
      }
     }

     
     if($data['password'] == '')
     {
      $error['password'] = "Mời bạn nhập mật khẩu !";
     }
     else
     {
      $data['password'] = MD5(postInput("password"));
     }

     
     if($data['phone'] == '')
     {
      $error['phone'] = "Mời bạn nhập số điện thoại !";
     }

     
     if($data['address'] == '')
     {
      $error['address'] = "Mời bạn nhập địa chỉ !";
     }

     if(empty($error))
     {
      $insert = $db->insert("users",$data);
      if($insert > 0)
      {
        $_SESSION['success'] = "Đăng kí thành công mời bạn đăng nhập !";
        header("location: dangnhap.php");
      }
      else
      {
        
      }
     }
   }
   
 ?>
 <?php require_once __DIR__. "/layouts/header.php"; ?>

      <div class="col-md-9 bor">

                        <section class="box-main1">
                           <h3 class="title-main"><a href="">Đăng kí thành viên</a> </h3>
                            <form action="" method="POST" class="form-horizontal formcustome" role= "form" style="margin-top: 20px">
                              <div class="form-group">
                                <label class="col-md-2 col-md-offset-1" > Tên thành viên</label>
                                    <div class="col-md-8">
                                      <input type="text" name="name" placeholder="" class="form-control" value="<?php echo $data['name'] ?>">
                                      <?php if(isset($error['name'])): ?>
                                        <p class="text-danger"><?php echo $error['name'] ?></p>
                                        <?php endif ?>
                                    </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 col-md-offset-1" > Email</label>
                                    <div class="col-md-8">
                                      <input type="email" name="email" placeholder="" class="form-control" value="<?php echo $data['email'] ?>">
                                      <?php if(isset($error['email'])): ?>
                                        <p class="text-danger"><?php echo $error['email'] ?></p>
                                        <?php endif ?>
                                    </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 col-md-offset-1" > Mật khẩu</label>
                                    <div class="col-md-8">
                                      <input type="password" name="password" placeholder="" class="form-control" value="<?php echo $data['password'] ?>">
                                      <?php if(isset($error['password'])): ?>
                                        <p class="text-danger"><?php echo $error['password'] ?></p>
                                        <?php endif ?>
                                    </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 col-md-offset-1" > Số điện thoại</label>
                                    <div class="col-md-8">
                                      <input type="number" name="phone" placeholder="" class="form-control" value="<?php echo $data['phone'] ?>">
                                      <?php if(isset($error['phone'])): ?>
                                        <p class="text-danger"><?php echo $error['phone'] ?></p>
                                        <?php endif ?>
                                    </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 col-md-offset-1" > Địa chỉ</label>
                                    <div class="col-md-8">
                                      <input type="text" name="address" placeholder="" class="form-control" value="<?php echo $data['address'] ?>">
                                      <?php if(isset($error['address'])): ?>
                                        <p class="text-danger"><?php echo $error['address'] ?></p>
                                        <?php endif ?>
                                    </div>
                              </div>

                              <button type="submit" class="btn btn-primary col-md-2 col-md-offset-9" style="margin-bottom: 20px;">Đăng kí</button>
                            </form>
                        </section>

          </div>
 <?php require_once __DIR__. "/layouts/footer.php"; ?>             

                