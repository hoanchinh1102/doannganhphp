<?php 
   $open = "category";

   require_once __DIR__. "/../../autoload/autoload.php";

   $category = $db->fetchAll("category");

    if(isset($_GET['page']))
   {
    $p = $_GET['page'];
   }
   else 
    {
      $p = 1;
    }

    $sql = "SELECT category.* FROM category ORDER BY ID DESC ";
    $category = $db->fetchJone('category',$sql,$p,4,true);

    if(isset($category['page']))
    {
      $sotrang = $category['page'];
      unset($category['page']);
    }


 ?>



<?php require_once __DIR__. "/../../layouts/header.php"; ?>
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                               Danh sach danh muc
                               <a href="add.php" class="btn btn-info">Them moi</a>
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i> Danh muc
                                </li>
                            </ol>
                        <div class="clearfix"></div>
                        <?php if(isset($_SESSION['success'])) :?>
                          <div class="alert alert-success">
                            <?php echo $_SESSION['success']; unset($_SESSION['success']) ?>
                           </div>
                        <?php endif ; ?>

                        <?php if(isset($_SESSION['error'])) :?>
                          <div class="alert alert-danger">
                            <?php echo $_SESSION['error']; unset($_SESSION['error']) ?>
                           </div>
                        <?php endif ; ?>
                    </div> 
                  </div>
                    <div class="row">
                      <div class="col-lg-12">
                      <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên danh mục</th>
                    <th>Mô tả</th>
                    <th>Trạng thái</th>
                    <th>Thời gian</th>
                    <th>Hoạt động</th>
                </tr>
            </thead>
            <tbody>
              <?php $stt = 1; foreach ($category as $item): ?> 
                <tr>
                    <td><?php echo $stt ?></td>
                    <td><?php echo $item['name'] ?></td>
                    <td><?php echo $item['slug'] ?></td>
                    <td>
                      <a href="home.php?id=<?php echo $item['id'] ?>" class = "btn btn-xs <?php echo $item['home'] == 1 ? 'btn-info' : 'btn-default' ?>">
                        <?php echo $item['home'] == 1 ? 'Hiển thị' : 'Không' ?>
                      </a>
                    </td>
                    <td><?php echo $item['created_at'] ?></td>
                    <td>
                      <a class="btn btn-xs btn-info" href="edit.php?id=<?php echo $item['id'] ?>">
                        <i class="fa fa-edit"></i>Sửa</a>
                      <a class="btn btn-xs btn-danger" href="delete.php?id=<?php echo $item['id'] ?>">
                        <i class = "fa fa-times"></i>Xóa</a>
                    </td>
                </tr>
              <?php $stt++ ;endforeach ?>
                   
            </tbody>
        </table>
      <div class="pull-right">
        <nav aria-label="Page navigation">
           <ul class="pagination">
              <li>
                <a href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
              </a>
              </li>
              <?php for( $i = 1; $i<= $sotrang; $i++) : ?>
                <?php
                if(isset($_GET['page']))
                {
                  $p= $_GET['page'];
                } 
                else
                {
                  $p= 1;
                }
                ?>
                <li class="<?php echo ($i== $p) ? 'active' : '' ?>" >
                  <a href="?page=<?php echo $i ;?>"><?php echo $i; ?></a>
                </li>
              <?php endfor; ?>
              <li>
                <a href="#" aria-label="Next">
                 <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
          </ul>
        </nav>
       </div>
                      </div>
                      </div>
                    </div>

                    <!-- /.row -->
<?php require_once __DIR__. "/../../layouts/footer.php"; ?>