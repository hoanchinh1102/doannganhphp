
<?php 

   require_once __DIR__. "/autoload/autoload.php";
   $sum = 0;
   if( ! isset($_SESSION['cart']) | count($_SESSION['cart']) == 0)
   {
    echo "<script>alert('Giỏ hàng trống');location.href='index.php'</script>";
   }

 ?>
 <?php require_once __DIR__. "/layouts/header.php"; ?>

      <div class="col-md-9 bor">

                        <section class="box-main1">
                            <h3 class="title-main"><a href=""> Giỏ hàng </a> </h3>
                            <table class="table table-hover">
                            	<thead>
                            		<tr>
                            			<th>STT</th>
                            			<th>Tên sản phẩm</th>
                            			<th>Hình ảnh</th>
                            			<th>Số lượng</th>
                            			<th>Giá</th>
                            			<th>Tổng tiền</th>
                            			<th>Thao tác</th>
                            		</tr>
                            	</thead>
                            	<tbody id="tbody" >
                            	<?php $stt = 1; foreach ($_SESSION['cart'] as $key => $value): ?>
                            	   <tr>
                            	   	<td><?php echo $stt ?></td>
                            	   	<td><?php echo $value['name'] ?></td>
                            	   	<td>
                            	   		<img src="<?php echo uploads() ?>/product/<?php echo $value['thunbar'] ?>" width="80px" height = "80px">
                            	   	</td>
                            	   	<td>
                                     <input type="number" name="qty" value="<?php echo $value['qty'] ?>" class="form-control qty" id = "qty" min ="0" >   
                                    </td>
                            	   	<td><?php echo formatPrice($value['price']) ?></td>
                            	   	<td><?php echo formatPrice($value['price'] * $value['qty']) ?></td>
                            	   	<td>
                            	   		<a href="remove.php?key=<?php echo $key ?>" class="btn btn-xs btn-danger">remove</a>
                            	   		<a href = "#"class="btn btn-xs btn-info updatecart" data-key=<?php echo $key ?>>update</a>
                            	   	</td>
                            	   </tr>
                                   <?php $sum += $value['price']*$value['qty']; $_SESSION['tongtien'] = $sum ; ?>
                            	<?php $stt ++ ; endforeach ?>
                            	</tbody>
                            </table>
                            <div id = "cach">
                                <a href="index.php" class="btn btn-danger">Mua hàng</a>
                                <a href="thanhtoan.php" class="btn btn-danger">Thanh toán</a>
                            </div>

                        </section>

          </div>
 <?php require_once __DIR__. "/layouts/footer.php"; ?>             

                