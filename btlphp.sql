-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2020 at 12:36 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `btlphp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `level` tinyint(4) DEFAULT 1,
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `address`, `email`, `password`, `phone`, `status`, `level`, `avatar`, `created_at`, `updated_at`) VALUES
(2, 'Bùi Xuân Hoàn', 'tỉnh Thái Bình', '151012023hoan@ou.edu.vn', '954e08e4a19f61a63c938abb85083e73', '0397669847', 1, 0, NULL, NULL, '2020-10-31 16:06:55'),
(4, 'Vu Thi Chinh', 'tỉnh Thái Bình', 'chinhcosy21@gmail.com', '954e08e4a19f61a63c938abb85083e73', '0397669847', 1, 0, NULL, NULL, NULL),
(5, 'Bùi Xuân Hoàn', 'tỉnh Thái Bình', '1751012023hoan@ou.edu.vn', '954e08e4a19f61a63c938abb85083e73', '0397669847', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home` tinyint(4) DEFAULT 0,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `images`, `banner`, `home`, `status`, `created_at`, `updated_at`) VALUES
(5, 'Iphone', 'iphone', NULL, NULL, 1, 1, '2020-10-10 07:04:54', '2020-10-25 13:57:29'),
(16, 'oppo', 'oppo', NULL, NULL, 1, 1, '2020-10-24 08:55:40', '2020-10-25 14:23:50'),
(17, 'samsung', 'samsung', NULL, NULL, 1, 1, '2020-10-24 09:01:25', '2020-10-31 13:02:12'),
(18, 'vertu', 'vertu', NULL, NULL, 1, 1, '2020-10-25 14:22:31', '2020-10-31 13:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` tinyint(4) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `transaction_id`, `product_id`, `qty`, `price`, `created_at`, `updated_at`) VALUES
(8, 5, 2, 1, 9000000, '2020-10-31 08:39:50', '2020-10-31 08:39:50'),
(9, 6, 16, 2, 10000000, '2020-10-31 08:59:16', '2020-10-31 08:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `sale` tinyint(4) DEFAULT 0,
  `thunbar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` int(11) NOT NULL DEFAULT 0,
  `head` int(11) DEFAULT 0,
  `view` int(11) DEFAULT 0,
  `hot` tinyint(4) DEFAULT 0,
  `pay` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `slug`, `price`, `sale`, `thunbar`, `category_id`, `content`, `number`, `head`, `view`, `hot`, `pay`, `created_at`, `updated_at`) VALUES
(2, 'Iphone 8plus', 'iphone-8plus', 9000000, 0, 'iphone 8plus.jpg', 5, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:28:27'),
(7, 'Iphone 11pro max hồng', 'iphone-11pro-max-hong', 20000000, 0, 'iphone-11-pro-max-512gb-gold-600x600.jpg', 5, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:28:33'),
(8, 'OPPO A92', 'oppo-a92', 6990000, 0, 'A92-Whi-1.jpg', 16, 'hàng trưng bày', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:28:36'),
(10, 'sasung galaxy A51', 'sasung-galaxy-a51', 7500000, 0, '1895160829.jpeg', 17, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:28:41'),
(16, 'Iphone 11 trang', 'iphone-11-trang', 10000000, 0, '(600x600)_600_iphone_11_pro_max_bac_didongmy_5.jpg', 5, 'bán', 100, 0, 0, 0, 1, NULL, '2020-11-23 11:28:48'),
(17, 'Iphone 11 ', 'iphone-11', 13000000, 0, 'iphone-11-red-600x600.jpg', 5, 'ban', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:28:55'),
(18, 'oppo A93', 'oppo-a93', 1000000, 0, 'oppo-a93-230520-060532-200x200.jpg', 16, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:29:00'),
(19, 'SAMSUNG A71', 'samsung-a71', 10000000, 0, 'samsung-galaxy-a71-195420-105424-200x200.jpg', 17, 'bán', 200, 0, 0, 0, 0, NULL, '2020-11-23 11:29:06'),
(21, 'SAMSUNG A21S', 'samsung-a21s', 10000000, 0, 'samsung-galaxy-a21s-3gb-055520-045548-600x600.jpg', 17, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:29:11'),
(23, 'Iphone Xs', 'iphone-xs', 15000000, 0, 'apple-iphone-xs-white-600x600.png', 5, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:29:18'),
(24, 'iphone XsMax', 'iphone-xsmax', 10000000, 0, 'iphone-xs-max-64gb-like-new_2.jpg', 5, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:29:23'),
(25, 'iphone SE', 'iphone-se', 10000000, 0, 'download.jpg', 5, 'bán', 100, 0, 0, 0, 0, NULL, '2020-11-23 11:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `amount`, `users_id`, `status`, `created_at`, `updated_at`) VALUES
(5, 9900000, 9, 1, '2020-10-31 08:39:50', '2020-10-31 08:39:50'),
(6, 22000000, 9, 1, '2020-10-31 08:59:16', '2020-10-31 08:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `address`, `password`, `avatar`, `status`, `token`, `created_at`, `updated_at`) VALUES
(9, 'Vũ Thị Chinh', 'chinhcosy21@gmail.com', '0397669847', 'tỉnh Thái Bình', '954e08e4a19f61a63c938abb85083e73', NULL, 1, NULL, NULL, NULL),
(10, 'Bùi Xuân Hoàn', '1751012023hoan@ou.edu.vn', '0397669847', 'tỉnh Thái Bình', '954e08e4a19f61a63c938abb85083e73', NULL, 1, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `transaction_id` (`transaction_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
